
#Function, written by Christian Tverberg
function Uptime()
{
	$bootTime = (Get-Date) - ([timespan]::FromMilliseconds([Math]::Abs([Environment]::TickCount)))
	$Current = Get-Date
	$timespan = $Current - $bootTime
	Write-output "Days: " 
	[console]::setcursorposition($([console]::Cursorleft + 10),$([console]::CursorTop - 1))
	Write-Output $timespan.Days, "Hours: "
	[console]::setcursorposition($([console]::Cursorleft + 10),$([console]::CursorTop - 1))
	Write-Output $timespan.Hours, "Minnutes: "
	[console]::setcursorposition($([console]::Cursorleft + 10),$([console]::CursorTop - 1))
	Write-Output $timespan.Minutes, "Seconds:	"
	[console]::setcursorposition($([console]::Cursorleft + 10),$([console]::CursorTop - 1))
	Write-Output $timespan.Seconds

}


Write-output "`n`t1 - Who am I and what is the name of this script?"
Write-output "`t2 - How long has it been since the last boot?"
Write-output "`t3 - How many processes and strings are there?"
Write-output "`t4 - How many context switches happened the last second?"
Write-output "`t5 - How much CPU time was used by the kernel and user mode the last second?"
Write-output "`t6 - How many interrupts happened the last second?"
Write-output "`t9 - Terminate. \n"

$choice = Read-Host "Choose a function "

while ($choice -ne 9) 
{
	Switch ($choice)

	{
	"1" 
    {
		Write-output "`n`t I am", $env:UserName, " and the name of this script is ", $MyInvocation.MyCommand.Name; 
		break
	}
	
   "2" 
	{
		Uptime
		break
	}
	
   "3" 
    {
		Write-Output "Number of processes: " (Get-Process | Select-Object -ExpandProperty id).Count
		
		$threadCount=0
		foreach ($i in Get-Process | Select-Object -ExpandProperty id)
		{
			$threadCount+= (Get-Process -id $i | Select-Object -ExpandProperty Threads).Count
		}
		Write-Output "`tNumber of threads: ", $threadCount
		break
	}
   "4" 
	{
		Write-Output "`tNumber of context switches last second: "
		Get-Counter -Counter "\System\Context Switches/sec" | Select-Object -ExpandProperty CounterSamples | Select-Object -ExpandProperty CookedValue
		break
	}   
	
	"5" 
    {
		$userTime=0
		$kernelTime=0
		$procs=Get-Process -ComputerName .
		foreach ($proc in $procs)
		{
			$UserTime += $proc.UserProcessorTime.TotalSeconds
			$kernelTime += $proc.PrivilegedProcessorTime.TotalSeconds
		}
		Write-Output "`tUsertime: " $userTime
		Write-Output "`tKernelTime: " $kernelTime
		break
	}
	
   "6" 
	{
		Write-Output "`tNumber of interrups last second: " $(((Get-Counter "\Processor(_total)\Interrupts/sec").CounterSamples).CookedValue)
		break
	}   
	
	"9" 
    {
		Write-Output "`tTerminating. "; 
		break
	}
   
   default 
   {
		"`n`t Uknkown command. "; 
		break
   }
}
$choice = Read-Host "Choose a function: "
}