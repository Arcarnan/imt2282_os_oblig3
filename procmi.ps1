#$id1=$args[0]
#$id2=$args[1]

#$Processes = get-process -computername $id1 | Group-Object -Property ProcessName | 
   # Format-Table Name, @{n='Mem (KB)';e={'{0:N0}' -f (($_.Group|Measure-Object WorkingSet -Sum).Sum / 1KB)};a='right'} -AutoSize

#Get-Process | Select-Object Name,@{Name='WorkingSet';Expression={($_.WorkingSet/1KB)}}

$date = Get-Date

for ($i = 0; $i -lt $args.count; $i++)
{
	$id = $args[$i]
	$filename = [string]$id + "-" + $date.Year + $date.Month + $date.Day + "-" + ".meminfo"
	$header = "******** Memory info on process with PID " + $id + " ********`n`n"

	#fetching values
	$VM =  Get-Process -id $id | Select-Object -ExpandProperty VirtualMemorySize #((get-process -id $id | Group-Object -Property ProcessName).Group|Measure-Object Workingset -sum).sum / 1MB
	$WS=(get-process -id $id | Select-Object -ExpandProperty WS)
	
	#prepare prints
	$VirtualMemory = "Total use of virtual memory: " + [math]::Round(($VM/ 1MB),2) + "MB"
	$workingset = "Size of working set: " + [math]::Round(($WS/ 1MB),2) + "MB"
	
	#make the file
	$header, $VirtualMemory, $workingset | out-file $filename
}