#processesAndThreads.ps1

$procs = Get-Process chrome | Select-Object -ExpandProperty Id

    Write-Output "`n"
    Write-Output "processName"
    [console]::setcursorposition($([console]::Cursorleft + 15),$([console]::CursorTop - 1))
    Write-Output "processId"
    [console]::setcursorposition($([console]::Cursorleft + 30),$([console]::CursorTop - 1))
    Write-Output "Threads"

foreach ($i in $procs)
{
    Write-Output "chrome"
    [console]::setcursorposition($([console]::Cursorleft + 15),$([console]::CursorTop - 1))
    Write-Output $i
    [console]::setcursorposition($([console]::Cursorleft + 30),$([console]::CursorTop - 1))
    Write-Output(Get-Process -Id $i | Select-Object -Expandproperty  Threads).Count
}