# OS - oblig 3 #

Creating various windows powershell scripts

Done by: Nataniel G�s�y (with some help from Christian Tverberg)

### PSScriptAnalyzer###


Invoke-ScriptAnalyzer myprocinfo.ps1
	- Changed from Write-Host to Write-Output

Invoke-ScriptAnalyzer processesAndThreads.ps1
	- Changed from Write-Host to Write-Output

Invoke-ScriptAnalyzer procmi.ps1
	- Changed from Write-Host to Write-Output
